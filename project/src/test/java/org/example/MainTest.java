package org.example;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MainTest {

	@Test
	public void getVersionTest() {
		assertEquals("master", Main.getVersion());
	}
}